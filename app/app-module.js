(function () {
  'use strict';

  /* @ngdoc object
   * @name spider
   * @description
   *
   */
  angular
    .module('spider', [
      'ui.router',
      'ui.bootstrap',
      'home',
      'test',
      'common',
      'angularMoment'
    ])
    ;
}());
