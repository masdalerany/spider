(function () {
  'use strict';
  angular
    .module('spider')
    .config(configUrl)
    // .config(configLocation)
    ;

  function configUrl($urlRouterProvider) {
    $urlRouterProvider.otherwise('/test');
  }

  function configLocation($locationProvider) {
    $locationProvider.html5Mode(true);
  }
}());
