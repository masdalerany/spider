(function () {
  'use strict';

  angular
    .module('test')
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('test', {
        url: '/test',
        templateUrl: 'test/test.tpl.html',
        controller: 'TestCtrl',
        controllerAs: 'test'
      });
  }
}());
