(function () {
  'use strict';

  /* @ngdoc object
   * @name test
   * @description
   *
   */
  angular
    .module('test', [
      'ui.router'
    ]);
}());
