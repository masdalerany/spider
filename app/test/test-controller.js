(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name test.controller:TestCtrl
   *
   * @description
   *
   */
  angular
    .module('test')
    .controller('TestCtrl', TestCtrl);

  function TestCtrl(Sensori) {
    var vm = this;
    vm.ctrlName = 'TestCtrl';
    vm.init = Init;

    vm.reload = function () {
      Sensori.ping().then(
        function (result) {
          vm.result = result;
        }
      );
    };

    function Init() {
      vm.reload();
    }

    vm.init();
  }
}());
