/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('TestCtrl', function () {
  var ctrl;

  beforeEach(module('test'));

  beforeEach(inject(function ($rootScope, $controller) {
    ctrl = $controller('TestCtrl');
  }));

  it('should have ctrlName as TestCtrl', function () {
    expect(ctrl.ctrlName).toEqual('TestCtrl');
  });
});
