/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('Sensori', function () {
  var factory;

  beforeEach(module('common'));

  beforeEach(inject(function (Sensori) {
    factory = Sensori;
  }));

  it('should have someValue be Sensori', function () {
    expect(factory.someValue).toEqual('Sensori');
  });

  it('should have someMethod return Sensori', function () {
    expect(factory.someMethod()).toEqual('Sensori');
  });
});
