(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name common.factory:Sensori
   *
   * @description
   *
   */
  angular
    .module('common')
    .factory('Sensori', Sensori);

  function Sensori(serverUrl, $http, $q) {
    var SensoriBase = {},
        apiURL = serverUrl,
        headers = {
          'Content-Type': 'application/json; charset=utf-8',
          Accept: 'application/json'
        };

    SensoriBase.ping = function () {
      return doServiceGet({}, 'ping');
    };

    return SensoriBase;

    function doServiceGet(data, uri) {
      var deferred = $q.defer();

      // console.debug('GET: ', data, uri);
      $http({
        method: 'GET',
        data: data,
        headers: headers,
        url: apiURL + uri
      }).success(function (D) {
        // console.debug('Success: ', D);
        deferred.resolve(D);
      }).error(function (D, S) {
        console.debug('GET errror', D, S);
        // DnbError.evalErrorResponse(D, S, deferred);
      })
      ;

      return deferred.promise;
    }
    function doServicePost(data, uri) {
      var deferred = $q.defer();

      console.debug('POST: ', data, uri);
      $http({
        method: 'POST', data: data, headers: headers,
        url: apiURL + uri
      }).success(function (D) {
        console.debug('Success: ', D);
        deferred.resolve(D);
      }).error(function (D, S) {
        console.debug('POST errror', D, S);
        // DnbError.evalErrorResponse(D, S, deferred);
      })
      ;

      return deferred.promise;
    }
  }
}());
