(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name common.constant:serverUrl
   *
   * @description
   *
   */
  angular
    .module('common')
    .constant('serverUrl', 'https://r11wazip8d.execute-api.eu-west-1.amazonaws.com/dev/');
}());
