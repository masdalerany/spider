/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('serverUrl', function () {
  var constant;

  beforeEach(module('common'));

  beforeEach(inject(function (serverUrl) {
    constant = serverUrl;
  }));

  it('should equal 0', function () {
    expect(constant).toBe(0);
  });
});
