/* global describe, beforeEach, it, browser, expect */
'use strict';

var TestPagePo = require('./test.po');

describe('Test page', function () {
  var testPage;

  beforeEach(function () {
    testPage = new TestPagePo();
    browser.get('/#/test');
  });

  it('should say TestCtrl', function () {
    expect(testPage.heading.getText()).toEqual('test');
    expect(testPage.text.getText()).toEqual('TestCtrl');
  });
});
