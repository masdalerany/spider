# Spider Sense, an IOT experiment

## Build a fast and intuitive user interface for accessing IOT data ##

- Check the results at [Sens at Mas d'Alerany](http://sens.masdalerany.com)
- Support us by staying at the most peaceful rural house of the world -- [Mas d'Alerany](http://www.masdalerany.com)
- Buy our delicious cherries from our brand -- [Cerima Cherries](http://www.cerimacherries.com)
 
### Functionalities ###

1. Support real-time data
2. Support aggregated data
3. Add Ploting
4. Handle Cache
5. Export data
6. Document Architecture

### Comming Functionalities ###
5. [TO-DO] Handle user access [AWS Cognito](https://aws.amazon.com/blogs/apn/managing-saas-users-with-amazon-cognito/?adbsc=APN_Blog_20160604_62363526&adbid=739132143863889922&adbpl=tw&adbpr=66780587)
6. [TO-DO] Add mapping support [Maps](https://developers.google.com/maps/)



*Generated with [ng-poly](https://github.com/dustinspecker/generator-ng-poly/tree/v0.11.5) version 0.11.5*

*Vcli 0.1 - Tivissa 2016*